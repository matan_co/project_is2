<?php

use yii\db\Migration;

class m170716_090040_create_task extends Migration
{
    public function up()
    {
		$this->createTable('task', [
            'id' => $this->primaryKey(),
			'name' => $this->string(),
			'description' => $this->text(),
			'project_id' => $this->integer()->notNull(),
			'plan_finish_date' => $this->date(),
			'finish_date' => $this->date(),
			'status' => $this->string()
		]);
		
		$this->createIndex(
			'idx-task-project_id',
			'task',
			'project_id'
		);
		
		$this->addForeignKey(
			'fk-task-project_id',
			'task',
			'project_id',
			'project',
			'id',
			'CASCADE'
		);
    }

    public function down()
    {
        $this->dropTable('task');
    }

}
