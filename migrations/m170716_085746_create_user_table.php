<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170716_085746_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string(),
            'password' => $this->string(),
            'authKey' => $this->string(),
			'firstName' => $this->string(),
			'lastName' => $this->string(),
			'created_at' => $this->date()
		]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
