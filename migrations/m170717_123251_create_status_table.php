<?php

use yii\db\Migration;

/**
 * Handles the creation of table `status`.
 */
class m170717_123251_create_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('status', [
            'id' => $this->primaryKey(),
			'status' => $this->string(),
			'created_at' => $this->date(),
			'updated_at' => $this->date()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('status');
    }
}
