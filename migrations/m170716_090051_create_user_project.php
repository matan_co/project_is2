<?php

use yii\db\Migration;

class m170716_090051_create_user_project extends Migration
{
    public function up()
    {
		$this->createTable('user_project', [
            'id' => $this->primaryKey(),
			'user_id' => $this->integer()->notNull(),
			'project_id' => $this->integer()->notNull(),
			'master' => $this->boolean()->defaultValue(false)
		]);
		
		$this->createIndex(
			'idx-user_project-project_id',
			'user_project',
			'project_id'
		);
		
		$this->createIndex(
			'idx-user_project-user_id',
			'user_project',
			'user_id'
		);
		
		$this->addForeignKey(
			'fk-user_project-user_id',
			'user_project',
			'user_id',
			'user',
			'id',
			'CASCADE'
		);
		
		$this->addForeignKey(
			'fk-user_project-project_id',
			'user_project',
			'project_id',
			'project',
			'id',
			'CASCADE'
		);
    }

    public function down()
    {
        $this->dropForeginKey(
			'fk-user_project-project_id',
			'user_project'
		);
		
		$this->dropIndex(
			'idx-user_project-project_id',
			'user_project'
		);
		
		$this->dropForeginKey(
			'fk-user_project-user_id',
			'user_project'
		);
		
		$this->dropIndex(
			'idx-user_project-user_id',
			'user_project'
		);
		
        $this->dropTable('user_project');
    }

}
