<?php

use yii\db\Migration;

class m170716_090107_create_user_task extends Migration
{
    public function up()
    {
		$this->createTable('user_task', [
            'id' => $this->primaryKey(),
			'user_id' => $this->integer()->notNull(),
			'task_id' => $this->integer()->notNull()
		]);
		
		$this->createIndex(
			'idx-user_task-task_id',
			'user_task',
			'task_id'
		);
		
		$this->createIndex(
			'idx-user_task-user_id',
			'user_task',
			'user_id'
		);
		
		$this->addForeignKey(
			'fk-user_task-user_id',
			'user_task',
			'user_id',
			'user',
			'id',
			'CASCADE'
		);
		
		$this->addForeignKey(
			'fk-user_task-task_id',
			'user_task',
			'task_id',
			'task',
			'id',
			'CASCADE'
		);
    }

    public function down()
    {
        $this->dropForeginKey(
			'fk-user_task-task_id',
			'user_task'
		);
		
		$this->dropIndex(
			'idx-user_task-task_id',
			'user_task'
		);
		
		$this->dropForeginKey(
			'fk-user_task-user_id',
			'user_task'
		);
		
		$this->dropIndex(
			'idx-user_task-user_id',
			'user_task'
		);
		
        $this->dropTable('user_task');
    }
}
