<?php

use yii\db\Migration;

class m170716_090033_create_project extends Migration
{
    public function up()
    {
		$this->createTable('project', [
            'id' => $this->primaryKey(),
			'name' => $this->string()
		]);
    }

    public function down()
    {
        $this->dropTable('project');
    }
}