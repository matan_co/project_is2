<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h2>Welcome To Project Managment System</h2>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Project</h2>

                <p>Create Projects, Update them, delete them and view them</p>

                <p><a class="btn btn-default" href="?r=project/index">View Projects</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Tasks</h2>

                <p>Create Tasks, Update them, delete them and view them</p>

                <p><a class="btn btn-default" href="?r=task/index">View Tasks</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Actions</h2>

                <p>Assign user to tasks, Assign user to projects</p>

                <p><a class="btn btn-default" href="#">Perform Actions</a></p>
            </div>
        </div>

    </div>
</div>
