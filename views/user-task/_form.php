<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Task;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\UserTask */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-task-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->dropDownList(User::getUsers()) ?>

    <?= $form->field($model, 'task_id')->dropDownList(Task::getTasks()) ?>  

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
