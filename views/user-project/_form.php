<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Standard;
use app\models\Project;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\UserProject */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-project-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<?= $form->field($model, 'user_id')->dropDownList(User::getUsers()) ?>  
	
	<?= $form->field($model, 'project_id')->dropDownList(Project::getProjects()) ?>  
	
    <?= $form->field($model, 'master')->dropDownList(array(1 => 'true', 0 => 'false')) ?> 

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
