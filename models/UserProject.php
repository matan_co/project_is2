<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;
use yii\db\QueryBuilder;

/**
 * This is the model class for table "user_project".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $project_id
 * @property integer $master
 */
class UserProject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'project_id'], 'required'],
            [['user_id', 'project_id', 'master'], 'integer'],
			[['master'], 'CheckMaster'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'project_id' => 'Project ID',
            'master' => 'Master',
        ];
    }
	
	public function getFindUser()
    {
		return  User::findOne($this->user_id);
    }
	
	public function getFindProject()
    {
		return  Project::findOne($this->project_id);
    }
	
	public function CheckMaster($attribute, $params)
	{
		$numOfMaster = (new \yii\db\Query())
			->from('user_project')
			->where(['master' => 1])
			->where(['<>' , 'id', $params->id])
			->count();
			
			
		if ($numOfMaster != 0){
		   $this->addError($attribute, 'Only 1 master can be active');
		}
		
		return true;
	}
	
	
}
