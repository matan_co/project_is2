<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $project_id
 * @property string $plan_finish_date
 * @property string $finish_date
 * @property string $status
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['project_id'], 'required'],
            [['project_id'], 'integer'],
            [['plan_finish_date', 'finish_date'], 'safe'],
            [['name', 'status'], 'string', 'max' => 255],
        ];
    }
	
	public static function getTasks()
	{
		$allTasks = self::find()->all();
		$tasks = ArrayHelper::
		map($allTasks, 'id', 'username');
		
		return $tasks;						
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'project_id' => 'Project ID',
            'plan_finish_date' => 'Plan Finish Date',
            'finish_date' => 'Finish Date',
            'status' => 'Status',
        ];
    }
}
